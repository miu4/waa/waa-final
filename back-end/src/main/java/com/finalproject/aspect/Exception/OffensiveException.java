package com.finalproject.aspect.Exception;

public class OffensiveException extends RuntimeException{
    public OffensiveException(String message) {
        super(message);
    }
}
